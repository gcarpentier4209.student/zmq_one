const express = require('express')
const app = express()
const port = 3000

var zmq = require("zeromq"),
sock = zmq.socket("push");
sock.bindSync("tcp://127.0.0.1:3002");


app.get('/', (req, res) => {
  res.send('Hello One!')
  console.log("sending test");
  sock.send("test");
})

app.listen(port, () => {
  console.log(` listening at http://localhost:${port}`)
})